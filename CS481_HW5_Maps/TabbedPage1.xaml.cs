﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Xamarin.Essentials;
using Xamarin.Forms.Maps;


namespace CS481_HW5_Maps
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {
        public TabbedPage1()
        {
            InitializeComponent();
            TabbedPage tabs = new TabbedPage();   //makes a tabbed page
            Children.Add(new Page1());   //each tab in this app represents a map with a list of locations(picker) that include pins with information
            Children.Add(new Page2());
            Children.Add(new Page3());
        }
    }
}