﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW5_Maps
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new TabbedPage1();  //used a tabbed page to display the three different maps 
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
