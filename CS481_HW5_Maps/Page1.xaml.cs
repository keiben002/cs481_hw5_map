﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace CS481_HW5_Maps
{
    public class pos
    {
        public string Name { get; set; } //data binding Name from page1.xaml picker section

    }


    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public ObservableCollection<pos> position_list1 { get; set; } //list for positions from picker

        public Page1()
        {
            InitializeComponent();
            Locations1();           //position call
            Pins1();                //pins call

        }


        //This function is a list of all the locations presented in the picker
        public void Locations1()            //observable collection list
        {

            position_list1 = new ObservableCollection<pos>()
            {
                new pos()
                {
                  Name = "Bishop's Orchards Farm Market & Winery",
                },
                new pos()
                {
                    Name = "Camp Sequassen",
                },
                new pos()
                {
                    Name = "Radio City Music Hall",
                },
                new pos()
                {
                    Name = "Rockefeller Center",
                }
            };

            posList.ItemsSource = position_list1;
        }

        private void Pins1()       
        {
            //all of these locations are based on the East Coast and I have also been to these locations
            var pin1 = new Pin()
            {
                Position = new Position(41.290053, -72.694607),
                Label = "Bishop's Orchards Farm Market & Winery",
                Address = "1355 Boston Post Rd, Guilford, CT 06437",
            };
            var pin2 = new Pin()
            {
                Position = new Position(41.877266, -73.049757),
                Label = "Camp Sequassen",
                Address = "791 West Hill Rd, New Hartford, CT 06057",
            };
            var pin3 = new Pin()
            {
                Position = new Position(40.760214, -73.979989),
                Label = "Radio City Music Hall",
                Address = "1260 6th Ave, New York, NY 10020",
            };
            var pin4 = new Pin()
            {
                Position = new Position(40.758892, -73.979024),
                Label = "Rockefeller Center",
                Address = "45 Rockefeller Plaza, New York, NY 10111",
            };
            Hy_Map.Pins.Add(pin1);
            Hy_Map.Pins.Add(pin2);
            Hy_Map.Pins.Add(pin3);
            Hy_Map.Pins.Add(pin4);

        }

        //Upon selecting a cell from the picker it will move to the cell's attached position
        //This position will be the same position from the Pins1() function according to the list
        //The picker is normally set to -1; functions as NULL until set to a value
        private void pList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            var count = picker.SelectedIndex;
            if (count != -1)
            {
                if (count == 0)
                {
                    Hy_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(41.290053, -72.694607), Distance.FromMiles(10)));

                }
                if (count == 1)
                {
                    Hy_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(41.877266, -73.049757), Distance.FromMiles(10)));
                }
                if (count == 2)
                {
                    Hy_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(40.760214, -73.979989), Distance.FromMiles(10)));
                }
                if (count == 3)
                {
                    Hy_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(40.758892, -73.979024), Distance.FromMiles(10)));
                }
            }

        }
    }
}