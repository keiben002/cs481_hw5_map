﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace CS481_HW5_Maps
{
    public class pos2
    {
        public string Name { get; set; } //data binding Name from page1.xaml picker section

    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public ObservableCollection<pos2> position_list2 { get; set; } //list for positions from picker
        public Page2()
        {
            InitializeComponent();
            Locations2();           //position call
            Pins2();                //pins call
        }

        public void Locations2()            //observable collection list
        {

            position_list2 = new ObservableCollection<pos2>()
            {
                //these locations are from the United States and are from the Pacific side of the country
                //I have also been to all of these places
                new pos2()
                {
                  Name = "Tip Top Meats",
                },
                new pos2()
                {
                    Name = "DisneyLand",
                },
                new pos2()
                {
                    Name = "BattleMage Brewing Company",
                },
                new pos2()
                {
                    Name = "Sequoia National Forest",
                }
            };

            posList2.ItemsSource = position_list2;
        }

        //This function contains all the pins used for the positions with their Name and Label information
        //as well as their postion on the map 
        private void Pins2()        
        {
            var pin1 = new Pin()
            {
                Position = new Position(33.120055, -117.318516),
                Label = "Tip Top Meats",
                Address = "6118 Paseo Del Norte, Carlsbad, CA 92011",
            };
            var pin2 = new Pin()
            {
                Position = new Position(33.812324, -117.919049),
                Label = "DisneyLand",
                Address = "1313 DisneyLand Dr, Anaheim, CA 92802",
            };
            var pin3 = new Pin()
            {
                Position = new Position(33.140563, -117.227328),
                Label = "BattleMage Brewing Company",
                Address = "2870 Scott St #102, Vista, CA 92081",
            };
            var pin4 = new Pin()
            {
                Position = new Position(36.481740, -118.567708),
                Label = "Sequoia National Park",
                Address = "47050 Generals Hwy, Three Rivers, CA 93271",
            };
            Street_Map.Pins.Add(pin1);
            Street_Map.Pins.Add(pin2);
            Street_Map.Pins.Add(pin3);
            Street_Map.Pins.Add(pin4);

        }

        //Upon selecting a cell from the picker it will move to the cell's attached position
        //This position will be the same position from the Pins2() function according to the list
        private void pList_SelectedIndexChanged2(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            var count = picker.SelectedIndex;
            if (count != -1)
            {
                if (count == 0)
                {
                    Street_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.120055, -117.318516), Distance.FromMiles(10)));

                }
                if (count == 1)
                {
                    Street_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.812324, -117.919049), Distance.FromMiles(10)));
                }
                if (count == 2)
                {
                    Street_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.140563, -117.227328), Distance.FromMiles(10)));
                }
                if (count == 3)
                {
                    Street_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(36.481740, -118.567708), Distance.FromMiles(10)));
                }
            }
        }


    }
}