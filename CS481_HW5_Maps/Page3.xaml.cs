﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace CS481_HW5_Maps
{
    public class pos3
    {
        public string Name { get; set; } //data binding Name from page1.xaml picker section

    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {

        public ObservableCollection<pos3> position_list3 { get; set; } //list for positions from picker

        public Page3()
        {
            InitializeComponent();
            Locations3();           //position call
            Pins3();                //pins call
        }

        //These locations are the names that are displayed from the picker list and stored in this observable collection
        public void Locations3()            //observable collection list
        {

            position_list3 = new ObservableCollection<pos3>()
            {
                //**All these locations are based in Ireland and are places that I have visited
                new pos3()
                {
                  Name = "Cliffs of Moher",
                },
                new pos3()
                {
                    Name = "Guinness Storehouse",
                },
                new pos3()
                {
                    Name = "House of Waterford Crystal",
                },
                new pos3()
                {
                    Name = "Blarney Stone",
                }
            };

            posList3.ItemsSource = position_list3;
        }

        //Each of these are pins that are attached to the picker list
        //They display the name and the address upon clicking on a pin
        private void Pins3()        
        {
            var pin1 = new Pin()
            {
                Position = new Position(52.973323, -9.429980),
                Label = "Cliffs of Moher",
                Address = "Lislorkan North, Liscannor, Co. Clare, V95 KN9T, Ireland",
            };
            var pin2 = new Pin()
            {
                Position = new Position(53.342035, -6.286674),
                Label = "Guinness Storehouse",
                Address = "The Liberties, Dublin 8, Ireland",
            };
            var pin3 = new Pin()
            {
                Position = new Position(52.259446, -7.106724),
                Label = "House of Waterford Crystal",
                Address = "28 The Mall, Waterford, Ireland",
            };
            var pin4 = new Pin()
            {
                Position = new Position(51.929216, -8.571047),
                Label = "Blarney Stone",
                Address = "Monacnapa, Blarney, Co. Cork, Ireland",
            };
            Sat_Map.Pins.Add(pin1);
            Sat_Map.Pins.Add(pin2);
            Sat_Map.Pins.Add(pin3);
            Sat_Map.Pins.Add(pin4);

        }


        //When a location is selected according to its slot [0-3] it will move the current location to the selected location on the map 
        private void posList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            var count = picker.SelectedIndex;
            if (count != -1)
            {
                if (count == 0)
                {
                    Sat_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(52.973323, -9.429980), Distance.FromMiles(10)));

                }
                if (count == 1)
                {
                    Sat_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(53.342035, -6.286674), Distance.FromMiles(10)));
                }
                if (count == 2)
                {
                    Sat_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(52.259446, -7.106724), Distance.FromMiles(10)));
                }
                if (count == 3)
                {
                    Sat_Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(51.929216, -8.571047), Distance.FromMiles(10)));
                }
            }
        }
    }
}